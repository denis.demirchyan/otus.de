﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Timers;

namespace Otus.DocumentsReceiver
{
    public class DocumentsReceiver : IDisposable
    {
        public event EventHandler DocumentsReady;
        public event EventHandler TimeoutOut;

        private readonly FileSystemWatcher _watcher = new() {EnableRaisingEvents = true};
        private readonly Timer _timer = new();

        private readonly List<string> _filenames;
        private string _targetDirectory;
        
        public DocumentsReceiver(List<string> filenames)
        {
            _filenames = filenames;
        }
        
        /// <summary>
        /// Проверка наличия заданных документов в определенной папке в указанный интервал времени
        /// </summary>
        /// <param name="targetDirectory">Папка с файлами</param>
        /// <param name="waitingInterval">Интервал времени, в течение которого будет происходить проверка</param>
        /// <exception cref="Exception">Ошибка при отсутствии файлов для поиска</exception>
        /// <exception cref="ArgumentException">Ошибка некорректно заданной папки</exception>
        public void Start(string targetDirectory, uint waitingInterval)
        {
            if (_filenames.Count == 0)
            {
                throw new Exception("Filenames is empty.");
            }
            if (!Directory.Exists(targetDirectory))
            {
                throw new ArgumentException($"Directory {targetDirectory} is not exist.");
            }

            _targetDirectory = targetDirectory;
            
            _watcher.Path = targetDirectory;
            _watcher.Changed += OnFilesChanged;
            _watcher.Renamed += OnFilesChanged;
            _watcher.Created += OnFilesChanged;
            _watcher.Deleted += OnFilesChanged;
            
            _timer.Interval = waitingInterval;
            _timer.Start();
            _timer.Elapsed += OnTimerElapsed;
        }
        
        public void Dispose()
        {
            _timer.Dispose();
            _watcher.Dispose();
        }

        private void OnFilesChanged(object sender, FileSystemEventArgs e)
        {
            var dirFiles = Directory.EnumerateFiles(_targetDirectory).ToList();
            if (_filenames.Count > dirFiles.Count) return;
            var fullFileNames = from filename in _filenames
                select Path.Combine(_targetDirectory, filename);
            if (fullFileNames.All(filename => dirFiles.Any(df => df == filename)))
            {
                OnDocumentsReady();
            }
        }
        
        private void OnTimerElapsed(object sender, ElapsedEventArgs e)
        {
            OnTimeoutOut();
        }
        
        protected virtual void OnDocumentsReady()
        {
            DocumentsReady?.Invoke(this, EventArgs.Empty);
            DisableEvents();
        }

        protected virtual void OnTimeoutOut()
        {
            TimeoutOut?.Invoke(this, EventArgs.Empty);
            DisableEvents();
        }

        private void DisableEvents()
        {
            _watcher.Changed -= OnFilesChanged;
            _watcher.Renamed -= OnFilesChanged;
            _watcher.Created -= OnFilesChanged;
            _watcher.Deleted -= OnFilesChanged;
            _timer.Elapsed -= OnTimerElapsed;
            _timer.Stop();
        }
    }
}