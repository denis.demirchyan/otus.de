﻿using System;
using System.Collections.Generic;

namespace Otus.DE
{
    class Program
    {
        static void Main(string[] args)
        {
            var dr = new DocumentsReceiver.DocumentsReceiver(new List<string>
                {"Паспорт.jpg", "Заявление.txt", "Фото.jpg"});
            
            dr.DocumentsReady += (_, _) => Console.WriteLine("Документы успешно получены.");
            dr.TimeoutOut += (_, _) => Console.WriteLine("Время ожидание превышено.");

            var dir = GetDirFromConsole();
            var interval = GetInervalFromConsole();
            
            dr.Start(dir, interval);
            
            Console.WriteLine("Нажмите любую клавишу, чтобы завершить работу программы.");
            Console.ReadKey();
        }

        private static string GetDirFromConsole()
        {
            Console.WriteLine("Введите путь к папке с файлами:");
            return Console.ReadLine();
        }

        private static uint GetInervalFromConsole()
        {
            Console.WriteLine("Введите время ожидания:");
            if(uint.TryParse(Console.ReadLine(), out var interval))
            {
                return interval;
            }
            Console.WriteLine("Некорректный интервал");
            return GetInervalFromConsole();
        }
    }
}